\chapter{Results} \label{Chapter03}

As we iterated in the previous chapter, the smaller the values for $GRV2$ and $GRV3$ are, the more accurate the computed model is since in the ideal scenario, they should disappear. Contrarily, this is not always the case; for example, in certain models, the estimated values for $GRV2$ or $GRV3$ do not represent a generally improved agreement in physical quantities in the code. In this chapter we will expand on the checks we performed to concur on the veracity of the above statements.

\section{Code CPU time}

First, for the work we will describe here we aimed at measuring the CPU time a process using the LALSimulation TOV solver takes in from start to end. As it is, we measured the CPU time desired for integrating the TOV, and the Virial equations, as well as the dimensionless tidal deformability parameter using $10$ points of integration. This was done for three Equations of State of the old EOS framework, and three Equations of State of the new EOS framework. The CPU time was measured using functions of the time.h header file, a standard file in the C programming language.

We built the code eight different times. Each time we changed the relative error in the gsl function that integrates the differential equations in the code. We used the interval $\left[10^{-9},10^{-2}\right]$, and a step of $10$. Then, as seen in Figs. \ref{CPU_old} and \ref{CPU_old}, we plotted the CPU time used for the same process for the eight different relative errors, for both the new and the old EOS frameworks. 

\newpage

    %\includegraphics[width=11cm]%, %height=6cm]
    
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]
    {fig/CPU_Time_Old_Framework.png}
    \caption{CPU times and relative errors from a sample of realistic tabulated EOSs of the old framework of the LALSimulation TOV solver.}
    \label{CPU_old}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/CPU_Time_New_Framework.png}
    \caption{Same as in Figure \ref{CPU_old}, but for the {\it new} EOS framework of the LALSimulation TOV solver.}
    \label{CPU_new}
\end{figure}


\newpage 
As seen in the figures mentioned, the CPU time taken by the same process of integrating the TOV and Virial differential equations for both old and new framework EOSs is larger for bigger values of relative errors used. Furthermore, for EOSs of the old framework the CPU time exhibits an almost semi-logarithmic behavior with respect to the relative accuracy of the TOV solver routine. The process is, as expected, quicker for smaller relative errors used, while using relative error values bigger the $10^{-6}$, the CPU time is always bigger than $10^{-2}\,\mathrm{sec}$.

As for EOSs of the old framework, for two of them (RG(SLy2) and PCPBSk24) the behavior is again semi-logarithmic. One difference is that for these EOSs the CPU time yielded with errors over $10^{-5}$ are above $10^{-2}\,\mathrm{sec}$. Another difference is that for the VGBCMR(D1M$\ast$) the CPU time measured for relative errors $10^{-4},\,10^{-3},\,10^{-2}$ is essentially the same and above $10^{-2}\,\mathrm{sec}$.

All in all, we can attest that the code additions we made and discussed in Chapter \ref{Chapter02} do not affect the timing of the LAL code. Indeed if one just considers the extent to which the new framework EOS files are different in size, the CPU time is almost exactly the same for integrating the differential equations using either new or old Equations of State tables.

\section{Accuracy in the numerical results}

As mentioned in a previous section, the addition of the calculation of the Virial identities was made in order to have an error indicator. Specifically, the 2D and 3D Virial identities serve as error indicators for the mass, the radius and the dimensionless tidal deformability. However, the relation between the value of a Virial identity - whether that is $GRV2$ or $GRV3$ - and the error on the values of the aforementioned variables is not a straightforward one. That is because in an algorithm like LALSimulation through which the value (and thus the error) of a quantity is being calculated, the dials that can alter that final value, introducing numerical errors in the calculations are many. As such, we had to cast a wide net in order to decipher which action or characteristic of the data themselves can create errors big enough that the Virial values are small.

\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/A_PP_DM_E_R.png}
    \caption{The relative error for the mass as a function of the TOV solver routine relative error at the maximum mass of an analytic piecewise polytropic Equation of State. The relative error data are plotted as solid points.}
    \label{A_PP_M}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/A_PP_DR_E_R.png}
    \caption{Same as in Fig. \ref{A_PP_M} but for the \emph{radius}, $R$.}
    \label{A_PP_R}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/A_PP_DL_E_R.png}
    \caption{Same as in Fig. \ref{A_PP_M} but for the \emph{dimensionless tidal deformability}, $\Lambda$. Here the errors are raised by almost two orders of magnitude.}
    \label{A_PP_L}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/A_PP_GRV2_E_R.png}
    \caption{The 2D Virial identity, $GRV2$, as a function of the relative accuracy of the TOV solver routine.}
    \label{A_PP_GRV2}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/A_PP_GRV3_E_R.png}
    \caption{Same as in Fig. \ref{A_PP_GRV2} but this time for the \emph{3D Virial identity}, $GRV3$.}
    \label{A_PP_GRV3}
\end{figure}

Using the expanded LALSimulation TOV solver we collected data regarding four different cases of Equation of State: an analytic piecewise polytrope, a tabulated polytrope, an old tabulated EOS ($\mathrm{AP1}$) and a new tabulated EOS ($\mathrm{RG(SLy2)}$). For these EOSs we assembled data sets by putting them through the TOV solver and saving the values for the mass, the radius, the dimesionless tidal deformability and the 2D and 3D Virial identities. These values correspond to the $M_{\mathrm{max}}$ of each case. Furthermore, we made a decision to change the relative accuracy of the TOV solver routine, $\varepsilon_{\mathrm{relative}}$, from $10^{-1}$ to $10^{-9}$ with a step of $10$. Thus, each EOS data file contains six columns - one for each variables - and nine lines - one for each case of the routine relative error. We will use these data sets in order to eludicate the accuracy of the calculation of variables using the LALSimulation TOV solver, and consequently untangle which values of the routine relative error leads to accurate enough results, that do not take too long to be calculated.

We used the data sets for each Equation of State in order to calculate the relative error in the results of each variable by electing the value at $\varepsilon_{relative}=10^{-9}$ as the most accurate. This can generally be written as
\begin{equation}
    \Delta x_i = \frac{|x_i - x_{10^{-9}}|}{x_{10^{-9}}}, \label{4.1}
\end{equation}
where $x_i$ is the value of a variable - here either $M$, $R$, or $\Lambda$ - at $i = 10^{-1},10^{-3},...,10^{-8}$ relative error of the TOV solver and $x_{10^{-9}}$ is the value of a variable using the $10^{-9}$ value for the relative error.

Having calculated the values for $\Delta x_i$ for each case of EOS and for every variable, we plot them with respect to the relative error in the TOV solver routine. We do the same for the values of $GRV2$ and $GRV3$. One small note that can be made here, and that a careful reader has mayhaps made on their own, is that for the plots of $\Delta x_i$ the data points are eight, whereas for the Virial identities nine. This of course happens since the relative errors here have been calculated using one of the nine values in each data set.

First, we will focus on the analytic piecewise polytrope case. The EOS here was procured via the "-Q" flag in the TOV solver, using $\log P_1=34.384$ and $\Gamma_1=2.909$, $\Gamma_2=2.246$ and $\Gamma_3=2.144$. We chose these values using a fit of the $\mathrm{SLy}$ tabulated EOS. In Fig. \ref{A_PP_M}, we plot the relative errors for the mass as a function of the TOV solver routine relative error. The relative error data are the solid points. We can see from the plot that there is an almost linear relationship between the two variables (note that the behavior is linear in log-log axes: it is logarithmic). That means that with bigger routine relative error - smaller accuracy locally - the value of the mass is less accurate. The opposite can be said for smaller values of the routine relative error: the smaller the value of the TOV solver error the more accurate the result in the mass. The same behavior can be seen in Figs. \ref{A_PP_R} and \ref{A_PP_L} for the radius and the dimesionless tidal deformability. One main difference is that for $\Lambda$ the relative errors produced are generally bigger by an order of magnitude two, while for $R$ the relative errors are slightly lesser. In general however, we can see that the errors are very small even when the relative accuracy in the TOV solver routine is bigger, for example with $\varepsilon_{TOV}=10^{-3}$ the relative errors in $M$, $R$ and $\Lambda$ are $\sim 10^{-5}$, $10^{-6}$, $10^{-2}$ respectively. 

Next, we plot in Figs. \ref{A_PP_GRV2} and \ref{A_PP_GRV3} the values of the Virial identities and the TOV solver routine accuracy. To begin with, while one would expect the values of the Virial identities to be more or less of the same magnitude as the routine accuracy, we can tell that even for the smallest value of the TOV routine relative accuracy - $10^{-1}$, $GRV2$ and $GRV3$ are two (almost three) orders of magnitude bigger. That is a behavior carried out throughout the range of values for the relative accuracy of the TOV solver routine. On the other hand, the results also showcase an almost linear behavior, that is expected of them: for smaller relative errors, so are the Virial identities smaller.

Following we study the case of a tabulated polytrope Equation of State. The polytrope here has $N=1.0$ and $K=100$. Data was collected for three forms of the table, for $200$, $100$ and $10000$ points. As in the case of the analytic piecewise polytrope, in Figs. \ref{T_P_MRL} and \ref{T_P_V} we plot the relative errors for $M$, $R$ and $\Lambda$ and $GRV2$, $GRV3$ respectively. The behavior of the relative errors in the $M$, $R$ and $\Lambda$ in this case indicates again a linear relationship between them and the relative accuracy of the TOV solver routine, even though the data are a bit more scattered along that line. The order of magnitude of the relative errors remain the same as in the analytic piecewise polytrope. A big difference, however, is seen in the Virial identities plots. We can concur that for the smaller tables - of $200$ and $1000$ points - both the Virial identities enter a plateau for a fairly small value of the relative accuracy of the TOV solver routine. Also, that plateau is at a fairly big value, at $\sim 10^{-4}$ and $\sim 10^{-6}$ for the $200$ and $1000$ case of the tabulated EOS respectively. 

This noteworthy behavior in the results leads us to believe that the interpolation scheme used in the code - a cubic spline - creates these errors that are carried through every step of the calculations. It is errors like this one that allow for the emergence of the plateau in the Virial identity results. One possible solution to this would be creating EOS tables of very big number of points (here the $10000$ point EOS yields the most desirable results) as well as using a more accurate interpolation method, such as Hermite interpolation that also has the benefit of upholding thermodynamic consistency. 

Up to this point, by just feeding the TOV solver two simple cases of EOS, we have reached some important conclusion. We discerned that there is an expected linear behavior in the relative errors for $M$, $R$ and $\Lambda$ as a function of the TOV solver routine relative accuracy. On the matter of the Virial identities, in the case of a tabulated polytrope EOS we also saw that the number of points used in the tabulated EOS are a very important dial that enters the algorithm and can alter their results. Now, we will focus on the remaining two choices of EOS both of whom are realistic tabulated EOSs. $\mathrm{AP1}$ is an EOS of the old framework and $\mathrm{RG(SLy2)}$ is an EOS of the new framework.

$\mathrm{AP1}$ is an old framework EOS and as such has $\sim 200$ data points. For $\mathrm{AP1}$ (Figs. \ref{AP1_M}-\ref{AP1_GRV3}) the same behavior for the relative errors in $M$, $R$ and $\Lambda$ is observed. More specifically the relative errors decrease linearly with the relative accuracy in the TOV solver routine and for $\Lambda$ the values for their relative errors are smaller by almost three orders of magnitude than those of $M$ and $R$. Regarding the Virial identities, the plateau after $10^{-3}$ resurfaces again here. Also, the plateau in the case of $\mathrm{AP1}$ is at lower values, at $\sim 7\cdot10^{-5}$ and $\sim 8\cdot10^{-3}$ for $GRV2$ and $GRV3$ respectively.

In the case of $\mathrm{RG(SLy2)}$ which is a \emph{new} framework tabulated EOS (Figs. \ref{RG_M}-\ref{RG_GRV3}) the points in the data file are $\sim 1000$. As expected, the relative errors in the mass, radius and dimensionless tidal deformability behave accordingly to the $\mathrm{AP1}$ EOS in both order of magnitude and linear relation to the TOV solver routine relative accuracy. Furthermore, in the Virial identities the plateau in their values is also observed, and again starts at $10^{-3}$ routine accuracy. 

We have extended the code for the LALSimulation TOV solver capabilities. After our additions, the person who wishes to run simulations of neutron star models can choose their own central rest-mass density or central energy density over which the TOV equations will be integrated. Furthermore, the use can now also choose to compute the 2D and 3D Virial identities. Their addition is of utmost importance since they are error indicators on the calculations. In general, one who wishes to create a huge amount of neutron star models, could choose to run all their possible EOS choices through the TOV and Virial - slower - routine and discern what choice of local realtive error in the TOV routine best fits each case. Then they would easily run the large number of simulations with the relative error changing each time and with respect to what EOS is being used, so that the results are both accurate enough and also not costly timewise. This can be done because as we showed, the mass, radius and tidal deformability all showcase a logarithmic behavior with respect to the TOV solver accuracy. So do the Virial identities for analytic piecewise polytropes and tabulated polytropes of many points. Lastly, we created a whole new Equation of State framework that can handle larger EOS data files, of many columns each representing a different variable. 

Of course, all this work was done with the aim that the resulting extended code for the LALSimulation TOV solver will be ready for use for the future Gravitational-Wave detections that are sure to be made during O4. To this end, we have already opened a review process in order for our additions to be incorporated to the wider code and be made available publicly for O4.

\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{fig/TAB_P_K_100_MRL_E_REL.png}
    \caption{The relative errors in mass (upper), the radius (middle) and dimensionless tidal deformability (lower) as a function of the relative accuracy of the TOV solver routine for a tabulated polytrope Equation of State. The data points for the relative errors are solid dots of color green for the $100$ point table, orange for the $1000$ point table and blue for the $10000$ point table.}
    \label{T_P_MRL}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{fig/TAB_P_K_100_VIRIAL_E_REL.png}
    \caption{Same as in Fig. \ref{T_P_MRL} but for $GRV2$ (upper) and $GRV3$ (lower).}
    \label{T_P_V}
\end{figure}

\newpage
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/AP1_DM_E_R.png}
    \caption{The relative error for the mass as a function of the TOV solver routine relative error at the maximum mass of the realistic old framework $\mathrm{AP1}$ Equation of State. The relative error data are plotted as solid points.}
    \label{AP1_M}
\end{figure}
 
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/AP1_DR_E_R.png}
    \caption{Same as in Fig. \ref{AP1_M} but for the \emph{radius}, $R$.}
    \label{AP1_R}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/AP1_DL_E_R.png}
    \caption{Same as in Fig. \ref{AP1_M} but for the \emph{dimensionless tidal deformability}, $\Lambda$. Here the errors are raised by almost three orders of magnitude.}
    \label{AP1_L}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/AP1_GRV2_E_R.png}
    \caption{The 2D Virial identity, $GRV2$, as a function of the relative accuracy of the TOV solver routine for the old framework realistic tabulated EOS $\mathrm{AP1}$.}
    \label{AP1_GRV2}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/AP1_GRV3_E_R.png}
    \caption{Same as in Fig. \ref{A_PP_GRV2} but this time for the \emph{3D Virial identity}, $GRV3$.}
    \label{AP1_GRV3}
\end{figure}

 \begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/RGSLY2_D_M_E_REL.png}
    \caption{The relative error for the mass as a function of the TOV solver routine relative error at the maximum mass of the realistic new framework $\mathrm{RG(SLy2)}$ Equation of State. The relative error data are plotted as solid points.}
    \label{RG_M}
\end{figure}
 
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/RGSLY2_D_R_E_REL.png}
    \caption{Same as in Fig. \ref{RG_M} but for the \emph{radius}, $R$.}
    \label{RG_R}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/RGSLY2_D_L_E_REL.png}
    \caption{Same as in Fig. \ref{RG_M} but for the \emph{dimensionless tidal deformability}, $\Lambda$. Here the errors are raised by almost three orders of magnitude.}
    \label{RG_L}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/RGSLY2_GRV2_E_REL.png}
    \caption{The 2D Virial identity, $GRV2$, as a function of the relative accuracy of the TOV solver routine for the \emph{new} framework realistic tabulated EOS $\mathrm{RG(SLy2)}$.}
    \label{RG_GRV2}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{fig/RGSLY2_GRV3_E_REL.png}
    \caption{Same as in Fig. \ref{RG_GRV2} but this time for the \emph{3D Virial identity}, $GRV3$.}
    \label{RG_GRV3}
\end{figure}