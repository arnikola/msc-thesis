\chapter{The \emph{LALSimulation} TOV solver} \label{Chapter02}

LALSimulation is a part of the LIGO Scientific Collaboration Algorithm Library, \emph{LALSuite} (\cite{lalsuite}). Its main use invloves implementation of routines for gravitational waveform and noise reduction. In this chapter we will focus on the download and installation process, the routines we worked with for this thesis as well as the changes that we made to the LALSimulation TOV solver code.

Our motivation for working on this specific code besides the upcoming O4 run of LIGO-Virgo, came mostly from the fact that LALSimulation has successfully gone through a review process in the past. Also, it has been used extensively in published research and strict syntax and structure rules must be upheld by anyone who wishes to add code development.

\section{Downloading and installing \emph{LALSuite}}

The LALSuite algorithm is written in the C programming language and is available for Linux and Mac operating systems. Before the user can access, use, modify the routines provied in the code, they have to download and build the code. The code is stored and can be found in the \href{https://git.ligo.org/lscsoft/lalsuite}{LALSuite} repository in the LIGO GitLab. The repository is public and therefore one does not have to have a LIGO account in order to access or clone it.

The maintainers of the repository suggest building LALSuite from source in a conda environment. This is also the way we built the code and recommend doing so. The conda environment, named \emph{lalsuite-dev}, contains all the main dependecies  needed in order to build the LAL algorithm. After downloading the environment .yml file from the repository, open the terminal and run 
\begin{lstlisting}[language=C]
 $ conda env create -f common/conda/environment.yml
\end{lstlisting}
Depending on the speed of your internet connection, pulling in dependencies can take some time. Following a successful environment setup, you may activate it using:
\begin{lstlisting}[language=C]
 $ conda activate lalsuite-dev
\end{lstlisting}

Then one must download the code itself from the repository. This is either done via the \emph{git clone} command using the link provided or manually by downloading the code as a .zip file and then extracting it. Once you have downloaded the code folder in your desktop you will navigate to it through the terminal. If you already have the lalsuite-dev environment activate it you may run
\begin{lstlisting}[language=C]
 $ ./00boot
\end{lstlisting}
This command is run for setting up the build and just for the first time you build LALSuite from source. The LALSuite build system, which controls which LALSuite components are built and with what features, must then be configured. You achieve this by running
\begin{lstlisting}[language=C]
 $ ./configure
\end{lstlisting}

Once the configuration of LALSuite has been completed, its time to build and install the code. This is done via running
\begin{lstlisting}[language=C]
 $ make
\end{lstlisting}
and when that finishes
\begin{lstlisting}[language=C]
 $ make install
\end{lstlisting}
In the future, every time you pull updates from the GitLab repository or make your own changes you need only run the last two steps described here. Reconfiguration and rebooting will be handled when needed automatically.

You will also need to source the paths of installation of the various LALSuite packages. Their paths can be easily found in terminal when the installating happens as they are printing quite distinctively on screen. Then the installation is complete and you can use LALSuite.

\section{The TOV solver}

The LALSimulation TOV solver is just one among the many routines offered in LALSimulation. The TOV solver is built around what we consider to be the epicenter executable file, \emph{ns-mass-radius.c}. Its smooth operation and accurate perfomance of the calculations dictated by the code is done by including several header files that set up various constants (like $G$ and constants associated with it like $G/c^2$) or other routines or other usage functions.

In anytime the user can simply type 
\begin{lstlisting}[language=C]
 $ lalsim-ns-mass-radius -h
\end{lstlisting}
in order to print the usage message in terminal.

The TOV solver is using the Lindblom formalism of the TOV equations, as seen in \ref{Chapter01} and Eqs. \ref{2.20}-\ref{2.21}, as well as Eqs. \ref{2.26}-\ref{2.28} for the calculation of the tidal Love number $k_2$. As discussed previously, the first step before anyone attempts to numerically integrate the TOV equations is to set up an Equation of State. In the LALSimulation TOV solver four choices are given for an EOS: (a.) 4-coefficient spectral decomposition (not discussed here), (b.) a polytropic EOS, (c.) a tabulated realistic EOS or (d.) an EOS from file. For each of these options of EOS an appropriate flag exists, that must be typed in terminal when calling the executable file in order to be parsed.

If one desires to use a polytropic EOS they have to type in the \emph{-P} flag for a single polytrope or \emph{-Q} for a 4-parameter piecwise polytrope. In any case they also must give values to a few other variables via the command line. If one looks at the \ref{2.29}-\ref{2.31} Eqs. they would see that the values that must be laid down before the integration in the case of a single polytrope are three. The polytropic exponent (also named the adiabatic index) $\Gamma$, is given a value by typing \emph{-G} and then the value. Then the value pressure at a reference density, by typing \emph{-p} and the appropriate number. Lastly, the reference density, by typing \emph{-r} and then adding the value.

For a 4-parameter piecewise polytrope, following \cite{Read2009}, there are four parameter that must be indicated after \emph{-Q}. As described in that paper, the low-density part is fit to the SLY4 EOS. First, the logarithm of pressure at $10^{17.7} \mathrm{kg/m^3}$ via the flag \emph{-q}. Then one must choose values for the three adiabatic indexes, $\Gamma_1$, $\Gamma_2$ and $\Gamma_3$ by typing \emph{-1}, \emph{-2} and \emph{-3}. Each flag must be followed by the value of each index. If a polytropic EOS has been chosen and the appropriate parameters correctly given then the program runs.

The choice of a single or a piecewise polytropic EOS gives a \emph{True} value to a variable which is then checked. A function that will create the polytropic EOS is then called. Of course, there are two functions available and depending on the choice of single or piecewise polytrope, one is called with the parameters the user gave in terminal parsed as arguments. The general logic in the code regarding the creation of an EOS is that it revolves around an EOS structure. The contents of the structure for any EOS are the values maximum pressure, $P_{\mathrm{max}}$, the maximum specific enthalpy, $h_{\mathrm{max}}$, the minimum specific enthalpy where the EOS becomes acausal, $h_{\mathrm{min,\,acausal}}$, several pointers to interpolating functions (for example $P=P(h)$ or $\epsilon=\epsilon(P)$) and functions that free memory and the EOS data themselves.

Once the function for the set up of the EOS structure is called (which is declared and defined in another file, here \emph{LALSimNeutronStarEOSPiecewisePolytrope.c}), a pointer to the EOS structre is declared. This pointer will be returned by the function once its jobs have been finished. Inside the function, pointers to the appropriate interpolating and freeing functions are set up and store in the EOS structure. Depending on the function that is called, thus also depending on what kind of polytrope is being calculated the codes computations will differ. For example, the code for the 4-parameter piecewise polytrope also checks whether to add another polytrope if the joining density is below the start of the last low density polytrope or above the end of the first high density polytrope. If it is needed an extra polytrope was used to join the low and high density regions. In the end a pointer to the EOS structure is returned, and we are essentially back in the starting file.

In the case of a tabulated EOS the user is asked to choose from a library of $65$ realistic EOSs. Typing \emph{-n} and then one of the available names in the library allows the user to choose an EOS table file to be used. Once that is done, a function that creates an equation of state structure from tabulated equation of state data of a known name is called. The code then goes to the \emph{LALSimNeutronStarEOSTabular.c} file where that function is defined. There, after checking the user-given name with the available EOS names (if there is a typo the program prints an error, as the given name is unrecognised) and once it finds the match it opens each data file. The installed EOS data files have the name format \emph{LALSimNeutronStarEOS(name).dat} and contain two columns with $\sim 150$ rows. Each column represents the data for $P$ and $\epsilon$. The EOS data file is read upon opening and the pressure and energy density data are stored in vector pointers. These pointers are then used as arguments in another function that allocates the data to the final EOS pointer.

In the aforementioned function several important processes occur. We will not dwell on the calculation of logarithms of quantities or the storing of them in the pointer function. Instead, we will discuss some other calculations that are done. First, the code checks whether the first rows of the pressure and energy density data are zero or not. If they are, they are excluded from further use. Then, the code has to calculate the specific enthalpy, which is the independent variable in the Lindblom form of the TOV equations. This is done using $dh/dP$ and integrating in log space,
\begin{equation*}
    h(P) = h(P_0) + \int_{\ln{P_0}}^{\ln{P}} \exp \left(\ln{P} + \ln{(dh/dP)}\right) d\ln{P}
\end{equation*}
where, $h(P_0) = P_0 / \left( \epsilon(P_0) + P_0\right)$ is the first point of integration. The integration is done using functions from the appropriate gsl library, like the interpolation between variables. 

From what we described this far regarding the tabulated EOSs used in LAL, one can see that all the quantities that are going to be used later in the code are derived from the same two basic variables. Those are the pressure and the energy desnity, each one of the columns of the EOS data files. That has been chosen in order to use smaller data files, and because the form of the TOV equations and the theory allows for derivation of the rest of the quantities from these two starting variables.

Once the EOS structure has been created, whether it is for data of a polytropic or a tabulated EOS, the pointer to it is returned. Back to the main function of the \emph{ns-mass-radius.c} file, the TOV integration can now be performed. To start, the code uses a fixed value for the central rest-mass density, $\log{\rho_c}=75.5$, a fixed value for the number of points that are going to be used, $N=100$, as well as the $\log{P_{max}}$ value that is obtained by the EOS, in order to create a grid of $P_c$ values for solving the TOV equations. Of course, the user can influence all this via selecting their desired number of points of integration, simply by adding the \emph{-N} flag in terminal when calling the TOV solver.

The code after calculating a value for the central pressure, calls the function that integrates the TOV equations (located in \emph{LALSimNeutronStarTOV.c}). The calculations are all done in gravitational units. In this function, all things that have to do with the integration of the equations can be found. At first, the power series for the equations are set up, based on Eqs. \ref{2.22} and the equations regarding the gravitational Love number $k_2$. The equations themselves are set up in two different functions inside the same file. As this is a initial values problem, first points for all the appropriate values are set up. Then a gsl function, advances the solution to a value of the specific enthalpy that is very close to the surface using an explicit embedded Runge-Kutta Prince-Dormand $(8, \,9)$ method. This function creates a new control object which will keep the local error on each step within an absolute error of zero and relative error of $10^{-6}$ with respect to the solution. Then, one final Euler step is used to get to the surface of the neutron star where $h=0$.

After the integration of the equations has been finished, and the values for the mass, $M$ and the radius, $R$ have been obtained, the compactness and $y$ as seen in \ref{2.26} are used to calculate $k_2$. The values for mass and radius are then turned to S.I. units and are returned along with $k_2$ back to the main function. There, an output function is used to print the results using a default output format of $R(\mathrm{km})$, $M(\mathrm{M_{\odot}})$. The output format can be changed by the user with the \emph{-F} flag. The user can if they want have the dimensionless tidal deformability or the gravitational love number $k_2$ for example. The process is repeated $N$ times and the results are each time printed in terminal.

One example of how a terminal window would look for a user who wants to solve the TOV equations, using the BSk19 tabulated EOS and 5 points is
\begin{lstlisting}[language=C]
 $ lalsim-ns-mass-radius -n BSK19 -N 5
1.201709911041519341e+01	3.647329369167171542e-01
1.106850709022855561e+01	9.792833082196663597e-01
1.026800948744378594e+01	1.681191607224219897e+00
8.708936599716377458e+00	1.847072776418876572e+00
7.348389360811195203e+00	1.608500633828228832e+00
\end{lstlisting}

When all the results have been printed in the terminal window, the code destroys the EOS pointer, frees all the allocated memory used, checks for memory leaks and if all is correct finishes.

Using the LALSimulation TOV solver we obtained the Mass-Radius diagram for the sample of EOSs we used in this thesis. The microcosmic physics of each EOS has been translated to the macroscopic quantities via the TOV equations and can be seen in \ref{fig mass-radius}. We can see that a polytropic piece - the same one each time - has been attached to the bottom of all EOSs in order to go to low densities that describe the crust of the neutron star.

\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{fig/EOS_Sample_M_R.png}
    \caption{A sample of realistic tabulated EOSs calculated under different physical theories is presented in the left. The mass-radius curves corresponding to the EOS are shown in the right panel. We produced this figure by using the LALSimulation TOV solver. The EOSs of the old framework are shown with broken lines, while the ones of the new framework with solid lines.}
    \label{fig mass-radius}
\end{figure}

\section{Expansion of the TOV solver}

As LIGO is preparing for its O4 run, several discussions have been held in the Extreme Matter Group throughout the year regarding the expansion of the LALSimulation TOV solver's capabilities. The results of these discussions lead to three new capabilities, that we studied, added to the code and that we present here.

\subsection{User chosen central density}

As previously discussed, in order to solve the TOV equations the solver used a fixed value for the central rest-mass density for the first step of the integration. The idea here was to add an option in the code with which a user will be able to choose this value for the central rest-mass density, $\rho_c$, or the central energy density, $\epsilon_c$. 

This was achieved by introducing four new interpolating functions in the code. Two for the polytropic EOS part, and two more for the tabulated EOSs. These functions were created since there was not an option in the code for translating values for either $\rho_c$ or $\epsilon_c$ to a value for pressure. The user given values for $\rho_c$ or $\epsilon_c$ are given in terminal after typing \emph{-d} or \emph{-e} and the number in $\mathrm{kg/m^3}$ and $\mathrm{J/m^3}$ respectively. When either of the new functions is called, very much like the already established interpolating functions of LALSimulation, takes the value of the respective quantity in S.I., transforms it to gravitational units, and then using a pointer from the EOS structure translates the value given to a value of pressure.

We provide two examples of what a terminal window using these new flags would look like, for a piecewise polytrope
\begin{lstlisting}[language=C]
 $ lalsim-ns-mass-radius -Q -q 32.504 -1 2.2 -2 1.33 \\
 -3 2.4 -d 7e17 -N 5
5.033207837732035728e+01	8.342498309981238869e-02
1.323064152904356838e+01	9.712105149464574094e-02
6.503581272264990609e+00	1.758337028983970041e-01
5.103729925787177102e+00	3.209657274027796769e-01
4.429070012753713925e+00	4.924417414312836083e-01
\end{lstlisting}
and for a tabulated EOS
\begin{lstlisting}[language=C]
 $ lalsim-ns-mass-radius -n BSK19 -e 6e34 -N 5
1.109882131658842042e+01	9.216359647216958439e-01
1.059031952654083142e+01	1.519771030312253801e+00
9.510598771044550759e+00	1.844865344133767016e+00
8.221543102181103180e+00	1.793543519515971241e+00
7.254217215527592799e+00	1.580123598099952398e+00
\end{lstlisting}

\subsection{2D and 3D Virial identities}

The following equation, called the Virial relation, is satisfied by equilibrium configurations in Newtonian gravity
\begin{equation}
2 T+3(\Gamma-1) U+W=0, \label{2.32}
\end{equation}
where $T$ is the rotational energy, $U$ is the internal energy and $W$ is the gravitational energy. The Virial theorem has proven valuable in physics and astrophysics, particularly in relation to the stability and equilibrium characteristics of dynamical systems. It has been used to evaluate the precision of numerically produced solutions (\cite{Ostriker}).

Bonazzola discovered the first analogous relations in general relativity (1973). Moreover, \cite{BG} found two virial identities in general relativity.
These identities are true for any spacetime that is asymptotically flat. With the use of these identities, we may calculate the numerical inaccuracy. Let's use the following definitions for the quantities $\lambda_2$ and $\lambda_3$:
\begin{equation}
\lambda_2 \equiv \frac{8 \pi \int_0^{+\infty} \int_0^\pi\left[P+(\varepsilon+P) \frac{v^2}{1-v^2}\right] \mathrm{e}^{2 \mu} r \mathrm{~d} r \mathrm{~d} \theta}{\int_0^{+\infty} \int_0^\pi\left[\partial \nu \partial \nu-\frac{3}{4} \mathrm{e}^{2 \psi-2 \nu} \partial \omega \partial \omega\right] r \mathrm{~d} r \mathrm{~d} \theta} \label{3.2}
\end{equation}
and,
\begin{equation}
\begin{aligned}
\lambda_3 \equiv & 4 \pi \int_0^{+\infty} \int_0^\pi\left[3 P+(\varepsilon+P) \frac{v^2}{1-v^2}\right] \mathrm{e}^{2 \mu+\psi} r \mathrm{~d} r \mathrm{~d} \theta \\
& \times\left\{\int _ { 0 } ^ { + \infty } \int _ { 0 } ^ { \pi } \left[\partial \nu \partial \nu-\frac{1}{2} \partial \mu \partial \psi\right.\right.\\
&+\frac{\mathrm{e}^{2 \mu-2 \psi}}{2} r \sin ^2 \theta\left(\frac{\partial \mu}{\partial r}+\frac{1}{r \tan \theta} \frac{\partial \mu}{\partial \theta}\right) \\
&+\frac{1}{4 r}\left(1-\mathrm{e}^{2 \mu-2 \psi} r^2 \sin ^2 \theta\right)\left(\frac{\partial \psi}{\partial r}+\frac{1}{r \tan \theta} \frac{\partial \psi}{\partial \theta}\right.\\
&\left.\left.\left.-\frac{1}{r \sin ^2 \theta}\right)-\frac{3}{8} \mathrm{e}^{2 \psi-2 \nu} \partial \omega \partial \omega\right] \mathrm{e}^\psi r \mathrm{~d} r \mathrm{~d} \theta\right\}^{-1}. \label{3.3}
\end{aligned}
\end{equation}
Here, $\partial \mu \partial \psi \equiv \frac{\partial \mu}{\partial r} \frac{\partial \psi}{\partial r}+\frac{1}{r^2} \frac{\partial \mu}{\partial \theta} \frac{\partial \psi}{\partial \theta}$.

Thanks to Charalampos Markakis we got the forms of $\lambda_2$ and $\lambda_3$ in the spherically-symmetric limit. We will approach them one at a time for simplicity reasons.

For $\lambda_2$ we have
\begin{equation}
    \lambda_2 = \frac{I_1}{I_2+I_3}, \label{3.4}
\end{equation}
where $I_1$, $I_2$ are integrals. They are written as
\begin{equation}
I_1 = \frac{8 \pi G}{c^2} \int_0^R  r\left[1-\frac{2 G m}{r c^2}\right]^{-1 / 2} P dr, \label{3.5}
\end{equation}
\begin{equation}
I_2 = \int_0^R r\left[1-\frac{2 G m}{r c^2}\right]^{-3 / 2}\left[G \frac{m+4 \pi r^3 P}{r^2 c^2}\right]^2 dr. \label{3.6}
\end{equation}
The $I_3$ quantity is
\begin{equation}
I_3=\left(1-\frac{G M}{R c^2}\right)\left(1-2 \frac{G M}{R c^2}\right)^{-1 / 2}-1, \label{3.7}
\end{equation}
and is a simple calculation between the final values for the mass and the radius of the neutron star and not an integral.

For $\lambda_3$, which follows exactly the same logic as $\lambda_2$, we have
\begin{equation}
    \lambda_3 = \frac{J_1}{J_2+J_3}, \label{3.8}
\end{equation}
Integrals $J_1$ and $J_2$ are
\begin{equation}
J_1 =  \frac{4 \pi G}{c^2} \int_0^R r^2\left[1-\frac{2 G m}{r c^2}\right]^{-1 / 2} 3 P dr, \label{3.9}
\end{equation}
\begin{equation}
\begin{array}{r} J_2 =  \int_0^R  {\left[1-\frac{2 G m}{r c^2}\right]^{-1 / 2}\left\{\left[1-\frac{2 G m}{r c^2}\right]^{-1}\left[G \frac{m+4 \pi r^3 P}{r c^2}\right]^2-\right.} \\
\left.\frac{1}{2}\left[\left[1-\frac{2 G m}{r c^2}\right]^{1 / 2}-1\right]^2\right\}dr.
\end{array}, \label{3.10}
\end{equation}
and 
\begin{equation}
J_3=R\left[\left(1-\frac{G M}{R c^2}\right)\left(1-2 \frac{G M}{R c^2}\right)^{-1 / 2}-1\right]. \label{3.11}
\end{equation}

Then, we define the 2D and 3D Virial identities as
\begin{equation}
\begin{aligned}
G R V 2 \equiv\left|1-\lambda_2\right|,\\
G R V 3 \equiv\left|1-\lambda_3\right|, \label{3.12}
\end{aligned}
\end{equation}
which following the same logic as in the Newtonian Virial theorem should be exactly zero. 

Having to do with numerical results, however, the values of the Virial tags will never be zero. Their values should be as small as possible. For this fact, we may select $GRV2$ and $GRV3$ as the error indicators for numerically derived solutions of the TOV equations, since accurate solutions for stationary problems meet the aforementioned relations. Be aware that $GRV3$ gives the star's outer layers more weight because of its three-dimensional nature. The Newtonian Virial Theorem is generalized relativistically in $GRV3$.

In order to add the calculation of the 2D and 3D Virial identities to the code, for the first time ever, we turned the integral form of Eqs. \ref{3.5}, \ref{3.6} and \ref{3.9}, \ref{3.10} to differential equations (DEs) with the specific enthalpy as the independent variable. These four DEs are written as
\begin{flalign}
\frac{dI_1}{dh}&=\frac{8\pi G}{c^2}r\left[1-\frac{2Gm}{rc^2}\right]^{-1/2}P\frac{dr}{dh}, \label{eq:3.13} \\
\frac{dI_2}{dh}&=r\left[1-\frac{2Gm}{rc^2}\right]^{-3/2}\left[G\frac{m+4\pi r^3P}{r^2c^2}\right]^2\frac{dr}{dh}, \label{eq:3.14} \\
\frac{dJ_1}{dh}&=\frac{4\pi G}{c^2}r^2\left[1-\frac{2Gm}{rc^2}\right]^{-1/2}3P\frac{dr}{dh}, \label{eq:3.15} \\
\frac{dJ_2}{dh}&=\left[1-\frac{2Gm}{rc^2}\right]^{-1/2}\bigg\{\left[1-\frac{2Gm}{rc^2}\right]^{-1}\left[G\frac{m+4\pi r^3P}{rc^2}\right]^2- \nonumber \\  &\frac{1}{2}\left[\left[1-\frac{2Gm}{rc^2}\right]^{1/2}-1\right]^2\bigg\}\frac{dr}{dh}. \label{eq:3.16}
\end{flalign}

The above four differential equations form the Virial system of differential equations. The $dr/dh$ derivative is taken to be as \ref{2.20}. As in the TOV equations, all four of the equations in the system are singular at the star's center. Thus, in order to solve them we calculated the Taylor expansions around $h=h_c$ to the second order:
\begin{flalign}
I_1(h) &= - 8\pi r_0^2 P_c \Delta h - \Delta h^2, \label{eq:3.17} \\
I_2(h) &= - 16\pi^2 r_0^4 P_c^2 \Delta h - 6\pi r_0^2 P_c \Delta h^2, \label{eq:3.18} \\
J_1(h) &= - 12\pi r_0^3 P_c \Delta h - 3 r_0 \Delta h^3, \label{eq:3.19} \\  
J_2(h) &= - 16\pi^2 r_0^5 P_c^2 \Delta h + 8 \pi^2 r_0^5 P_c^2 \Delta h^2. \label{eq:3.20}
\end{flalign}
where $r_0$ is the calculation of the radius around $h=h_c$.

In order for the code to calculate the 2D and 3D Virial tags, we first chose to duplicate the existing routine that just solves the TOV equations and leave it unchanged. In the new routine, we added the \ref{eq:3.13}-\ref{eq:3.16} system of differential equations in order to be solved concurrently with the TOV equations. In other words, this new TOV$+$Virial routine has $6\,$DEs $=2\,$(TOV DEs)$ + 4\,$(Virial DEs). The solution follows more or less the same procedure as in the simple TOV solution routine. The exception is that besides the new DEs, the calculations of \ref{3.7} and \ref{3.9} has been also added after the values for the mass and the radius has been obtained. In the output the Virial tags are printed after the radius and the mass. If a user wants to calculate (and subsequently print) the Virial identities the just have to add the \emph{-U} flag in terminal, with no required argument.

One example is,
\begin{lstlisting}[language=C]
 $ lalsim-ns-mass-radius -n BSK19 -d 7e17 -U -N 5
1.10377e+01 1.03571e+00 3.21854e-03 3.04879e-03
1.04679e+01 1.59122e+00 2.46718e-03 2.40199e-03
9.37709e+00 1.85466e+00 1.54916e-03 1.54093e-03
8.14785e+00 1.78239e+00 7.84743e-04 7.87827e-04
7.24013e+00 1.57566e+00 3.80987e-04 3.53061e-04
\end{lstlisting}

As it is seen from the example above, the values of the Virial tags are not zero - as expected. Instead, they are very small. Their value and magnitude are discussed more in detail in the next chapter.

\subsection{New EOS framework}

One other change we worked on for the TOV solver was the tabulated EOS framework. As mentioned before, the available LALSimulation package makes use of $65$ tabulated EOS from various physical backgrounds, each calculated using different theory. However, internally in LIGO arose an uncertainty for the accuracy of the EOSs themselves, especially how their low-density part had been handled. For this reason it was agreed that a new EOS framework should be created in order to incorporate new EOS tables that were provided (and will be provided in the future) by Micaela Oertel and her team.

When beginning to solve this issue we had to think about the differences the new tables had in comparison to the old ones. For reference, the old EOS tables had two columns: one for pressure, one for energy density and about $\sim 150$ data points on average for each column. The new EOS reference table that we were provided with (for the RG(SLY2) EOS) has $9$ columns and $1231$ data points on each one. The format of the reference table was decided after numerous in-group discussions. So we can see that the new EOS table format already outdoes the old ones in both size and number of quantities involved. The RG(SLY2) table has columns for the baryon number density, $n_B(\mathrm{fm^{-3}})$, the energy density, $\epsilon(\mathrm{g/cm^{3}})$, the pressure, $P(\mathrm{dyn/cm^{2}})$, the baryon chemical potential, $\mu_B(\mathrm{MeV})$, the electron chemical potential, $\mu_e(\mathrm{MeV})$, the specific enthalpy, $h$, the electron fraction, $Y_e$, the speed of sound squared, $c_s^2$, and a column for the recalculated baryon number density.

Several quantities that were otherwise calculated after the EOS table had been loaded and the EOS structure created, like the speed of sound or more importantly the specific enthalpy, are built-in in the new EOS tables. Our code was based around the logic of the old framework but with the facts that the new tables would be bigger than the old ones, that several new quantities would have to be loaded and stored in an EOS structure and many calculations of quantities could now be skipped. 

The biggest addition is a new structure that houses the data, and the function pointers of the new EOS tables was created. Moreover, a trigger variable has been added that distinguishes whether a new or an old table is being handled and then tells the functions that perform interpolation which kind of data structure they should use, pull from and give results to. 

Accessing the new EOS framework does not require typing a specific flag in terminal. Instead, when giving a name of a tabulated EOS as argument using the \emph{-n} flag, the user should simply type in the name of an EOS that belongs to the new framework. This was done in order to keep the use of the old EOS tables intact for reference. Lastly, of course the new EOS tables can be freely be put through the rigors any old EOS table faced, such as the TOV integration or the calculation of the Virial identities. 

It should be noted that at the time of writing this thesis, only one table had been made available for the new framework. The RG(SLY2) EOS.

\begin{lstlisting}[language=C]
 $ lalsim-ns-mass-radius -n RGSLY2 -d 7e17 -N 5
1.188652059088177460e+01	1.228429638271954172e+00
1.166277521484264135e+01	1.569109799069216882e+00
1.125234617342466414e+01	1.844821041552365593e+00
1.066895561001428305e+01	2.008805442439049571e+00
9.981921470907151317e+00	2.053143796745568572e+00
\end{lstlisting}