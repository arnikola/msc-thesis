\chapter{Neutron Stars} \label{Chapter01}
This chapter begins with a brief historical overview of the concept of neutron stars as well as the subsequent theoretical and observational advancements that resulted from this concept. Some information and equations presented here can be found in \cite{vidana2018short}. The reader is encouraged to read into the article for a more in depth discussion on the subject.

\section{Brief Historical Overview}
\cite{Baade1934} first suggested the existence of neutron stars, just two years after \cite{Chadwick1932} discovered the neutron, though it has been suggested that Landau already posited the possibility of stars more compact than white dwarfs containing very dense matter in 1931 (\cite{Yakovlev2012}). A huge object made mostly of neutrons at a very high density would be far more gravitationally bound than a star, according to Baade and Zwicky. They also proposed that supernova explosions may produce such objects. \cite{Tolman} derived the equations that describe the structure of a static star with spherical symmetry in General Relativity as well as performed the first theoretical calculation of the equilibrium conditions of neutron stars and their properties assuming an ideal gas of free neutrons at high density, concurrently, though seperately from \cite{OV}, who also contributed to this work. It's curious to note that Chandrasekhar and von Neumann found the identical hydrostatic equilibrium equations in 1934, even though that they didn't publish their results. Oppenheimer and Volkoff discovered that stable static neutron stars could not be more massive than $\sim0.7M_{\odot}$, which is significantly less than the $\sim1.44M_{\odot}$, the famous Chandrasekhar mass limit for white dwarfs.

The low value of the maximum mass, $M_{\mathrm{max}}$, determined by Oppenheimer and Volkoff is a result of the straightforward depiction they provided of the state of matter of the neutron star interior in terms of non-interacting neutrons, and it is evidence that the nuclear forces have a crucial role to play in determining the right structure of these objects. After the Second World War, considerable work was done to create a more accurate Equation Of State (EOS) for neutron star matter. Around 1960, it became apparent that neutron stars' interiors may possibly contain muons, mesons, or hyperons, in addition to neutrons, protons, and electrons. First, \cite{Cameron} investigated the qualitative consequences of hyperons in neutron stars. The first in-depth computations of the EOS and composition of an equilibrated mixture of degenerate free Fermi gases of baryons, mesons, and leptons by \cite{Saakyan} came shortly after this research. A few years later, \cite{Tsuruta} used phenomenological interactions to analyze the impact of baryon-baryon forces on the dense matter EoS with hyperons. Although significant progress has been made in our understanding of the properties of matter under the extreme conditions found in the interior of neutron stars, the accurate behavior of the nuclear EOS at very high densities is still unknown. Many authors have followed in the footsteps of all these pioneers.

Even though neutron stars were theoretically predicted to exist in the middle of the 1930s, the astronomical community didn't pay any attention to them for roughly 30 years. One argument frequently used to dismiss the hypothesis of neutron stars was that, due to their small surface area, their leftover thermal radiation would be too feeble to be seen with optical telescopes at astronomical distances in compared to regular stars. However, since the \cite{Giacconi} discovery of Sco X-1, the first cosmic X-ray source of extrasolar origin, in rocket experiments interest in neutron stars has been greatly increased.

Many satellites with X-ray and -ray telescopes on board were launched during the 1980s, 1990s, and 2000s, and more will be launched in the coming years in order to see neutron stars.

The current generation of gravitational observatories, including the ground-based Advanced LIGO (\emph{Laser Interferometer Gravitational-Wave Observatory}) and Advanced VIRGO (from the \emph{European Gravitational Observatory}) and the prospective LISA (\emph{Laser Interferometer Space Antenna}) mission of the European Space Agency, are needed to see GWs, which provide a novel method of studying neutron stars.

\section{Perfect Fluids}

\subsection{The stress-energy tensor}

A perfect fluid is essentially a model of a big assembly of particles where the macroscopic distribution of mass can be described by a continuous energy density, $\epsilon$. One can impose a local thermodynamic equilibrium on the presumption that the mean free path of the tiny particles is short relative to the scale on which density changes occur, meaning they clash frequently. To an observer travelling at the fluid's average velocity, $u^{\alpha}$, the collisions will randomly disperse the local particle velocities, making the particle distribution look locally isotropic. As a result, we may establish a stress-energy tensor of the following form:
\begin{equation}
    T^{\alpha\beta}=\epsilon u^{\alpha}u^{\beta}+pq^{\alpha\beta}. \label{2.1}
\end{equation}

Here, the rotational scalars $\epsilon := T^{\alpha \beta}u_{\alpha}u_{\beta}$ and $P := \frac{1}{3} q_{\gamma \delta} T^{\gamma \delta}$ are the only zero parts of $T^{\alpha \beta}$ and correspond to the total energy density and the pressure as measured by a comoving observer. Then, $q^{\alpha \beta}=g^{\alpha \beta} + u^{\alpha}u^{\beta}$ is the projection operator orthogonal to $u^{\alpha}$ and $g_{\alpha \beta}$ is the metric.

\subsection{Thermodynamics}

With a given fixed rest mass of $m_B$ per baryon, we define $n$ as the baryon number density. Therefore, the rest-mass density is represented by
\begin{equation}
    \rho:=m_Bn. \label{2.2}
\end{equation}

One has to identify the parameters that will be used in order to describe the thermodynamic characteristics of matter in a compact object. There is a significant variety of thermodynamic quantities, including $(\rho,\,P,\,\epsilon,\,T)$, where $T$ is the temperature. Naturally, the parameters used rely on the precise physics that we wish to probe. In this work, we focus only on the scenario in which the energy density and pressure are determined by two variables $\rho$, and the specific entropy (entropy per unit rest mass $m_0$) $s$:
\begin{equation}
    \begin{array}{c}
    \epsilon=\epsilon(\rho, s),\\ \label{2.3}
    P=P(\rho, s).
    \end{array}
\end{equation}

The first law of thermodynamics takes the form:
\begin{equation}
    d\epsilon=\rho Tds + hd\rho, \label{2.4}
\end{equation}
where $h$ is the specific enthalpy (enthalpy per unit rest mass),
\begin{equation}
    h:=\frac{\epsilon+P}{\rho}. \label{2.5}
\end{equation}
From the definitions of \ref{2.5} and \ref{2.4} one can find that:
\begin{equation}
    \begin{aligned}
d h &=\frac{d \epsilon}{\rho}+\frac{d P}{\rho}-\frac{\epsilon+P}{\rho^2} d \rho=T d s+h \frac{d \rho}{\rho}+\frac{d P}{\rho}-h \frac{d \rho}{\rho}, \\
&=T d s+\frac{d P}{\rho}, \\
\Rightarrow d \ln h &=\frac{T}{h} d s+\frac{d P}{\epsilon+P}. \label{2.6}
\end{aligned}
\end{equation}

Another definition worth making is if one considers an homentropic star. In this case the first law of thermodynamics, \ref{2.4}, takes the form
\begin{equation}
    d\epsilon = hd\rho, \label{2.7}
\end{equation}
and using \ref{2.6} and the fact that at low densities the gas is non-relativistic (i.e. $\epsilon/\rho=1$ at $P=0$), the specific enthalpy is also given by
\begin{equation}
    h=\exp \left(\int_0^P \frac{d P}{\epsilon+P}\right), \label{2.8}
\end{equation}
or if we define the enthalpy ($H$) we get
\begin{equation}
    H \equiv \ln \left(\frac{\varepsilon+P}{\rho}\right), \label{2.9}
\end{equation}
which is the Gibbs-Duhem relation that implies that \begin{equation}
    \frac{\mathrm{d} P}{\mathrm{~d} H}=\varepsilon+P. \label{2.10}
\end{equation}

\subsection{Fluid dynamics and conservation laws}

Five variables are required to adequately characterize the state of a perfect fluid since we took into account a two-parameter EOS. Because we have the normalization $u_{\alpha}u^{\alpha}=-1$, those parameters may be regarded to be the three independent components of $u^{\alpha}$, as well as $\epsilon$ and $P$. The diminishing divergence of the stress-energy tensor then describes the fluid's dynamical development
\begin{equation}
    \nabla_\beta T^{\alpha \beta}=0, \label{2.11}
\end{equation}
which comes from the Bianchi identities, the Einstein field equations $G^{\alpha \beta}=8 \pi T^{\alpha \beta}$ and the conservation of baryons
\begin{equation}
    \nabla_\alpha\left(\rho u^\alpha\right)=0. \label{2.12}
\end{equation}
The projection of Eq. \ref{2.11} along $u^{\alpha}$ leads to the \emph{conservation of energy}, while the projecion of the same equation normal to the fluid velocity yields the \emph{relativistic Euler equation}.

For the \emph{conservation of energy}:
\begin{equation*}
    \begin{aligned}
    0=u_\alpha \nabla_\beta T^{\alpha \beta} &=u_\alpha \nabla_\beta\left[\epsilon u^\alpha u^\beta+P q^{\alpha \beta}\right], \\
    &=-\nabla_\beta\left(\epsilon u^\beta\right)-P \nabla_\beta u^\beta,
    \end{aligned}
\end{equation*}
\begin{equation}
    \Rightarrow \nabla_\beta\left(\epsilon u^\beta\right) = -P \nabla_\beta u^\beta. \label{2.13}
\end{equation}

For the \emph{relativistic Euler equation}:
\begin{equation*}
    \begin{aligned}
    0=q^\alpha_\gamma \nabla_\beta T^{\beta \gamma} &=q^\alpha_\gamma \nabla_\beta\left[\epsilon u^\beta u^\gamma+P q^{\beta \gamma}\right], \\
    &=\epsilon u^\beta \nabla_\beta u^\alpha + q^{\alpha \beta} \nabla_\beta P + P u^\beta \nabla_\beta u^\alpha,
    \end{aligned}
\end{equation*}
\begin{equation}
    \Rightarrow (\epsilon + P)u^\beta \nabla_\beta u^\alpha = - q^{\alpha \beta} \nabla_\beta P. \label{2.14}
\end{equation}

\section{Tolman-Oppenheimer-Volkoff equations}

By combining Euler's and Poisson's equations with the well-known differential equation for mass, in the Newtonian theory it is possible to describe the equilibrium of a star. Of course, selecting an equation of state is a necessary step in the procedure. Neutron stars, though, are relativistic objects and this fact cannot be disregarded in their situation. As mentioned before, Tolman and Oppenheimer with Volkoff concurrently though separately formed a corresponding set of differential equations. Subsequently this set of equations was named TOV in their honor. 

One must establish a metric for the star's interior in order to derive these equations. The most prevalent metric for a spherically symmetric and static spacetime, according to Birkhoff's theorem, is
\begin{equation}
    ds^2=-\exp^{2\nu(r)} + \left(1-\frac{2m(r)}{r}\right)^{-1}dr^2 + r^2 \left(d\theta^2 + sin^2\theta d\phi^2\right), \label{2.15}
\end{equation}
which describes the star's interior. In the aforementioned relation, $\nu(r)$, also known as the metric potential, is a direct counterpart to the Newtonian gravitational potential and $m(r)$ stands for the gravitational mass enclosed inside a radius $r$.

It is important to note that the Schwarzschild metric, which is once again established as the sole metric representing such a physical system by Birkhoff's theorem, is the exterior metric of this configuration. The two answers must coincide in the border, therefore one should exercise caution.

One must carryout some algebra to determine the Einstein tensor $G_{\alpha \beta}$ which corresponds to the metric \ref{2.15} and employ Eq. \ref{2.1} as the stress-energy tensor, in order to extract the TOV equations. The gravitational mass, $m(r)$, differential equation is derived from $G_{tt} = 8T_{tt}$, while the metric potential, $\nu(r)$, differential equation is supplied from $G_{rr} = 8T_{rr}$. We arrive at the final equation involving the derivative of pressure, $P$, using the principle of energy conservation.

Thus, the TOV system of equations is:
\begin{flalign}
        &\frac{d P}{d r}=-\frac{(\epsilon+P)\left(m+4 \pi r^{3} P\right)}{r(r-2 m)}, \label{2.16}\\
        &\frac{d m}{d r}=4 \pi r^{2} \epsilon, \label{2.17}\\
        &\frac{d \nu}{d r}=-\frac{2}{\epsilon+P} \frac{d P}{d r}=\frac{2\left(m+4 \pi r^{3} P\right)}{r(r-2 m)}, \label{2.18}\\
        &P=P(\epsilon). \label{2.19}
\end{flalign}
Of course, the EOS has been added to the systen (Eq. \ref{2.19}) as it is a necessary component in order to complete the system of equations. 

When we take into account that these equations directly relate the star mass contained inside a specific radius to the pressure and energy density of an EOS, their significance becomes clear. Once $P = P(\epsilon)$ has been determined, one must integrate the TOV equations from center at $r=0$, where the pressure is equal to the central pressure, $P_c$, and the energy density is equal to the central energy density, $\epsilon_c$, to the surface at $r=R$, where $P=0$ and the enclosed mass is equal to the entire mass, $m=M$. The $\nu$ quantity can be given any central value, like $\nu(0)=-1$. To put it another way, the TOV equations provide a map between the nuclear physics as they are descirbed in the EOS (microcosm) and characteristics of neutron stars that can be measured through astronomical observations (macrocosm).

\subsection{The Lindblom formalism}

\cite{Lindblom} showed that the map provided by the TOV equations may be inverted. Using the function $h(P)$ as it is defined in Eq. \ref{2.8}, we can reexpress Eq. \ref{2.16} as $dh/dr$. Then, one can use $m$ and $r$ as independent variables and $h$ as the dependent variable in the equations. Also, \cite{Hartle} showed that in any non-singular stellar model, $r>2m$. From \ref{2.8} we see that $dh/dr\leq0$ and so $h$ is a monotonically decreasing function of $r$. As such, the roles of $r,\,h$ as independent or dependent variables can be interchanged. 

The TOV equations can then be written in terms of those variables as
\begin{flalign}
        &\frac{d r}{d h}=-\frac{r\left(r-2m\right)}{m+4\pi r^3 P}, \label{2.20}\\
        &\frac{d m}{d h}=-\frac{4\pi \rho r^3\left(r-2m\right)}{m+4\pi r^3 P}. \label{2.21}
\end{flalign}
The values for $P(h)$ and $\rho(h)$ can be taken either from Eqs. \ref{2.19} and \ref{2.18} or from a tabulated EOS that has these values already calculated. 

In comparison to the original, this variation of the TOV equations provides a number of benefits. Both the radius and the mass are introduced as dependent variables. As a result, the total mass, $M$ and total radius, $R$ are just the boundary values of the $m(h)$ and $r(h)$ functions at the star's surface. By doing this, finding the surface is no longer dependent on solving the equation $P(R)=0$. Since $dr/dh$ is finite there as opposed to $dr/dp$, these equations are simpler to numerically integrate close to the surface of the star.

Like the conventional TOV equations, the converted TOV equations are singular at the star's center. The non-singular solutions close to $h=h_c$ must thus be evaluated analytically. The first two non-trivial terms in the power series solutions for $r(h)$ and $m(h)$ close to $h=h_c$ are as follows:
\begin{equation}
\begin{aligned}
r(h)=& {\left[\frac{3\left(h_c-h\right)}{2 \pi\left(\rho_c+3 P_c\right)}\right]^{1 / 2} } \\
& \times\left\{1-\frac{1}{4}\left[\rho_c-3 P_c-\frac{3}{5}\left(\frac{d \rho}{d h}\right)_c\right] \frac{h_c-h}{\rho_c+3 P_c}\right\}, \\
& m(h)=\frac{4 \pi}{3} \rho_c r^3(h)\left[1-\frac{3}{5}\left(\frac{d \rho}{d h}\right)_c \frac{h_c-h}{\rho_c}\right]. \label{2.22}
\end{aligned}
\end{equation}
Here, the coefficients of the series are written as functions of $h_c$, $P_c=P(h_c)$ and $\rho_c=\rho(h_c)$.

The Lindblom formalism of the TOV equations offers a practical advantage when numerically integrating them. After the specification of the EOS, the power series determine $r(h)$ and $m(h)$ in a small neighborhood around the star's center. Then, a numerical integration scheme may be implemented in order to advance the solution of the equations to the remainder of the domain, $h_c\geq h\geq 0$.

\section{Tidal deformability of a neutron star}

Any extended object that might be put in an external field that is spatially inhomogeneous will encounter various forces across its range. The outcome is a tidal interaction, the theory of which is well-known for celestial bodies that are Newtonian. This section introduces the neutron star's tidal deformability parameter.

A neutron star's tidal deformability is a single quantity that expresses how easily the star is deformed by an external tidal field. A big tidal deformability value often indicates a bigger, less compact star that is easily malleable. On the other hand, a star that has a lower tidal deformability parameter is more compact, smaller, and harder to deform. The induced quadrupole $Q_{ij}$ to the perturbing tidal field $E_{ij}$ is what is mathematically referred to as the tidal deformability for both Newtonian and relativistic stars:
\begin{equation}
\lambda \equiv-\frac{Q_{i j}}{\mathcal{E}_{i j}} \label{2.23}
\end{equation}

The aforementioned equation and dimensionality considerations imply that $\lambda$ is a sensitive function of the neutron star's radius. The second spatial derivative of the external field, which yields units of inverse length squared, $[L]^{-2}$, is how the perturbing tidal field $E_{ij}$ is defined in the Newtonian limit. The units of the quadrupole moment are $[L]^3$. Overall, it is anticipated that $\lambda = \kappa R^5$, where $\kappa$ is a dimensionless constant. Following established conventions, the tidal deformability is defined as
\begin{equation}
\lambda=\frac{2}{3} k_2 R^5, \label{2.24}
\end{equation}
where $k_2$ is the gravitational Love number, which depending on the choice of EOS takes values in the range $0.2-0.3$. The importance of the tidal deformability parameters becomes clear when one considers that through both the Love number $k_2$ and the star's radius $R$, the neutron star's interior structure leaves its mark on the tidal deformability. In this way, tidal deformability is a supplementary probe of neutron star structure that provides extra information beyond pure radius observations.

The dimensionless tidal deformability, which is also often employed, is defined as
\begin{equation}
\Lambda \equiv \frac{\lambda}{M^5}=\frac{2}{3} k_2 \frac{R^5}{M^5}=\frac{2}{3} k_2 C^{-5}, \label{2.25}
\end{equation}
where $C\equiv M/R$ is the compactness of the star. 

\subsection{Computing the tidal deformability}

A single unperturbed neutron star will remain static and spherically symmetric, if we do not take into account the star's rotation. As mentioned before, the Schwarzschild metric will provide the outside spacetime according to Birkhoff's theorem. However, if the star is subjected to an external tidal field, the star will deform, and this will have an impact on spacetime.

The tidal deformability of a neutron star of a specific mass can be determined by computing the metric in the asymptotic regime using the Einstein equations, extracting the corresponding $r$ order terms, and taking their ratio given a proposed equation of state, the perturbed metric, and the definition of Eq. \ref{2.23}. \cite{Hinderer} gets an expression for $\lambda$ (or alternatively $k_2$) by enforcing continuity of the metric and its derivatives throughout the neutron star's surface and extending the metric solution asymptotically. The calculation is explained fully in \cite{Hinderer} and should be read for a more detailed look. As a consequence, an equation for the gravitational Love number $k_2$ is given:
\begin{equation}
\begin{aligned}
k_2=& \frac{8 C^5}{5}(1-2 C)^2[2+2 C(y-1)-y] \\
& \times\{2 C[6-3 y+3 C(5 y-8)]\\
&+4 C^3\left[13-11 y+C(3 y-2)+2 C^2(1+y)\right] \\
&\left.+3(1-2 C)^2[2-y+2 C(y-1)] \ln (1-2 C)\right\}^{-1}, \label{2.26}
\end{aligned}
\end{equation}
where $y$ is a quantity that depends on the value on $R$ of the metric function, $H(R)$ and its derivative, $\beta(R)=dH/dr|_{r=R}$, as $y=R\beta(R)/H(R)$.

In \cite{Damour} the form of a single second order radial differential equation for the metric variable $H$ is calculated in detail. It has the form
\begin{equation}
H^{\prime \prime}+C_1 H^{\prime}+C_0 H=0, \label{2.27}
\end{equation}
where at the stationary limit
\begin{equation}
\begin{aligned}
C_1=& \frac{2}{r}+\frac{1}{2}\left(\nu^{\prime}-\lambda^{\prime}\right)=\frac{2}{r}+e^\lambda\left[\frac{2 m}{r^2}+4 \pi r(P-\epsilon)\right] \\
C_0=& e^\lambda\left[-\frac{\ell(\ell+1)}{r^2}+4 \pi(\epsilon+P) \frac{d \epsilon}{d P}+4 \pi(\epsilon+P)\right] \\
&+\nu^{\prime \prime}+\left(\nu^{\prime}\right)^2+\frac{1}{2 r}\left(2-r \nu^{\prime}\right)\left(3 \nu^{\prime}+\lambda^{\prime}\right) \\
=& e^\lambda\left[-\frac{\ell(\ell+1)}{r^2}+4 \pi(\epsilon+P) \frac{d \epsilon}{d P}+4 \pi(5 \epsilon+9 P)\right] \\
&-\left(\nu^{\prime}\right)^2. \label{2.28}
\end{aligned}
\end{equation}
For reference, $l$ is the multipolar order and $e^{\lambda(r)} \equiv\left(1-\frac{2 m(r)}{r}\right)^{-1}$.

Thus, in order for someone to obtain the gravitational Love number $k_2$ (or by extent the dimensionless tidal deformability parameter $\Lambda$) they would first have to set up and numerically integrate Eq. \ref{2.27} given the values for $m$ and $r$ in each step from the integration of the TOV equations. Then, they would be able to calculate $y$ and lastly $k_2$.

As mentioned in \cite{Chatziioannou}, a natural environment for tidal interactions is between the components of a binary neutron star system. The two stars are gradually inching closer to one another during the early phases of the coalescence, releasing gravitational waves as they do so. The two stars are sufficiently separated in this early inspiral stage that tidal interactions are insignificant, and the signal that is released is identical to that of two inspiraling black holes with the same masses and spins. The neutron stars will eventually grow so near to one another due to energy loss that tidal interactions will be significant. Each neutron star will be affected by the gravitational tidal field created by its partner, as was mentioned above. Its shape will be adiabatically warped as a result; the tidal bulge in each star faces the companion because the time scale associated with this deformation is quicker than the orbital motion that changes the external tidal field.

The two binary components' generated tidal quadrupole moment will have an impact on how the system develops in the future. The leading-order contribution to the quadrupole moment for a binary corresponds to the motion of the binary orbit. The produced tidal quadrupole moment on the shapes of each star in neutron star binaries, however, yields a subleading contribution that directly affects the gravitational wave signal.

\section{Equation of State}

Because it is highly challenging to explore particle interactions beyond nuclear matter density from nuclear experiments or from nuclear theories, the physical state of neutron star matter is still not fully known. Given this circumstance, using the macroscopic quantities of neutron stars is a viable strategy to investigate the behavior of very high density matter. Particularly, the softness of the EOS at extremely high densities has a major role in determining the mass and rotational period of neutron stars. As a result, empirical constraints combined with theoretical models may be useful in reconstructing the equation of state of very high density materials.

As mentioned before, whether someone wants to integrate the TOV equations or to altogether calculate the gravitational Love number $k_2$, one essential part in the process is knowledge of the Equation of State. What the EOS offers is a (or rather many) phenomenological relationship between thermodynamic variables of a neutron star. For example we previously saw a more general use of a pressure-energy relation $P=P(\epsilon)$. EOSs where the dependent variable, here $P$, is a function of one independent variable, here $\epsilon$, are called barotropic. The EOS, which for the reason mentioned is in nature indispensable when thermodynamically or hydrodynamically describing a system, is discussed in this section.

\subsection{Polytropes}

A special case of EOSs, first introduced by \cite{Tooper}, is the polytropic EOS. A general form of the polytropic EOS is
\begin{equation}
P=K \rho^{\Gamma}=K \rho^{1+\frac{1}{n}}, \label{2.29}
\end{equation}
where $\Gamma$ is the polytropic exponent and is constant, $K$ is the polytropic constant and $n$ is the polytropic index.

The rise in pressure and density toward the star's center is adiabatic if the gradual composition change is ignored, and the first rule \ref{2.4}, with $ds = 0$, is then applicable
\begin{equation}
d \epsilon=\frac{\epsilon+P}{\rho} d \rho. \label{2.30}
\end{equation}
Then the energy and pressure relation follows in the form $d \frac{\epsilon}{\rho}=\frac{P}{\rho^2} d \rho$. If we demand $\lim _{P \rightarrow 0} \frac{\epsilon}{\rho}=1$,
\begin{equation}
\begin{gathered}
\frac{\epsilon}{\rho}=1+\int_0^\rho K \rho^{\Gamma-2} d \rho=1+K \frac{\rho^{\Gamma-1}}{\Gamma-1}, \\
\epsilon=\rho+\frac{P}{\Gamma-1} . \label{2.31}
\end{gathered}
\end{equation}
Despite not being as realistic as tabulated EOSs, polytropic EOSs are useful for double-checking numerical codes.

\subsection{Realistic Equations of State}

The inside of a neutron star contains matter at extreme conditions. Matter in neutron star cores is cold and ultradense, with considerable neutron and proton number asymmetry and enormous chemical potentials. The core density can go as high as $\sim 10 \rho_{sat}$ (\cite{ozelfreire}), where $\rho_{sat}$ in the nuclear saturation density. Given that neutrons overlap geometrically at $\sim 4 \rho_{sat}$, matter at these extremely high densities may not only be made up of nucleons but may also include a broad range of hadronic degrees of freedom. Transitions to non-nucleonic forms of matter are anticipated when nucleon overlap increases. For instance, it becomes more likely that quark degrees of freedom and quark components will gradually start to spread throughout the system as seen in \cite{Alford_2005}. Weak interactions in the cold neutron-star cores can result in states of matter with a high degree of strangeness, suggesting that ultradense matter may also include strange quarks in forms other than kaons. Suggestions of hyperons (\cite{Saakyan}) and hybrid stars with free quarks (\cite{Collins}) are just a few of the hypotheses that have been put out thus far.

The equation of state of neutron star matter has been calculated using a variety of different methodologies. \cite{LS} provide thorough summaries of the procedures and specifics of nuclear physics. 

% Here, we give an overview of the six tabulated EOSs that are used for results in this thesis. These are PCPBSk24, VGBCMR(D1M$\ast$), RG(SLY2), AP1, ALF1 and another version of the SLY2 EOS. All the data files for the first three EOSs were provided by Micael Oertel.

% \emph{PCPBSk24} Details on the EoS model can be found in \cite{Pearson}. This table corresponds to the zero temperature unified EOS for cold non-accreting neutron stars in beta equilibrium based on the Brussels-Montreal energy- density functional BSk24. The outer crust was calculated using the Hartree-Fock-Bogoliubov atomic mass table HFB-24, except when experimental values were available. The inner crust was computed using the 4th-order Extended Thomas-Fermi (ETF) method with proton shell and pairing corrections added perturbatively via the Strutinsky integral (SI); the nucleon distributions were parametrized using damped Fermi profiles and the Coulomb energy was calculated within the Wigner-Seitz (WS) approximation. Although the EOS was originally calculated ignoring nuclear pastas, their presence in neutron-star crust was later discussed. The core was assumed to be made up by an admixture of neutrons and protons neutralised by electrons and possibly by muons.

% \emph{VGBCMR(D1M$\ast$)} This EOS table corresponds to the data and model in \cite{Mondal}. The Gogny D1M$\ast$ effective interaction is used. The EOS in the inner crust is obtained with the Wigner-Seitz approximation in the Variational Wigner-Kirkwood approach along with the Strutinsky integral method, which allows one to estimate in a perturbative way the proton shell and pairing corrections. For the outer crust, the EOS is determined basically by the nuclear masses, which are taken from the experiments, wherever they are available, or by HFB calculations performed with this new force if the experimental masses are not known. The leptonic sector accounts for electrons and muons.

